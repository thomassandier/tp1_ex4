# Tester la classe
from calculator.calculator import SimpleComplexCalculator
complex1 = [4.67, 5.89]  # Premier nombre complexe
complex2 = [2.34, 3.45]  # Deuxième nombre complexe

calculator = SimpleComplexCalculator()

print(
    "Somme :", calculator.complex_sum(complex1, complex2)
)  # Affiche la somme des deux nombres complexes
print(
    "Différence :", calculator.complex_subtract(complex1, complex2)
)  # Affiche la différence des deux nombres complexes
print(
    "Produit :", calculator.complex_multiply(complex1, complex2)
)  # Affiche le produit des deux nombres complexes
print(
    "Division :", calculator.complex_divide(complex1, complex2)
)  # Affiche la division des deux nombres complexes
